package ru.god.regex;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, предназначенный для поиска конкретных фрагментов
 * в текстовом файле и дальнейшей работы с ними
 *
 * @author Горбачева, 16ИТ18к
 */
public class FindIdentifier {
    public static void main(String[] args) {
        writing(finding(reading()));
    }

    /**
     * Метод, осуществляющий чтение данных из файла
     *
     * @return List со строками из файла
     */
    private static List<String> reading() {
        List<String> list = null;
        try {
            list = Files.readAllLines(Paths.get("src\\identifier.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Метод, осуществляющий поиск идентификаторов
     *
     * @param list с исходными данными
     * @return List, содержащий только идентификаторы
     */
    private static List<String> finding(List<String> list) {
        Pattern pattern = Pattern.compile("[^A-Z_a-z][^A-Z_a-z0-9]*");
        Matcher matcher;
        for (String element : list) {
            matcher = pattern.matcher(element);
            while (matcher.find()) {
                list.set(list.indexOf(element), matcher.replaceAll("\n"));
            }
        }
        return list;
    }

    /**
     * Метод, позволяющий записать данные в файл
     *
     * @param list - строка для записи
     */
    private static void writing(List<String> list) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\identifier.txt"))) {
            for (String string : list) {
                bufferedWriter.write(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
